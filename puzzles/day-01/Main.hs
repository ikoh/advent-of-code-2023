module Main where

type CorruptedLine = String
type FixedLine = String
type IntAsString = String

main :: IO ()
main = do
    raw <- readFile "./data/day_01.txt"
    let parsed = words raw :: [String]

    -- Answer for part 1 (53386)
    print . sum $ map solvePart1 parsed

solvePart1 :: CorruptedLine -> Int
solvePart1 xs = read . getFirstAndLast $ removeLetters xs
  where
    removeLetters :: CorruptedLine -> FixedLine
    removeLetters = filter (\x -> x `notElem` ['a' .. 'z'])

    getFirstAndLast :: FixedLine -> IntAsString
    getFirstAndLast x = [head x, last x]