# AoC 2023 Day 5

Right, we’re dealing with some big numbers here. **Use the `Integer` type! Maybe also use the `Text` type rather than the default `String`!** It may even be worth considering `Data.Text.Lazy.Read` rather than its strict counterpart, as described in the [documentation](https://hackage.haskell.org/package/text-2.1/docs/Data-Text.html).

Part 1 is about inner joining our way to the final table, which we have to create by ourselves. Note that join keys may have to be created based on the target table: if the target table’s keys have a **greater range** than the source keys, this means that, in the source table, there are values that have no mapping in the target table. Then we use the following rule from the instructions (based on the given example):

> Any source numbers that **aren’t mapped** correspond to the **same** destination number. So, seed number 10 corresponds to soil number 10.

**Do not mess up the ordering of the columns:**

- Column 1: _destination_ range start
- Column 2: _source_ range start
- Column 3: range length

## Part 1

I reckon that there has got to be a more intelligent way of joining the tables (in the sense of not killing my machine’s RAM), seeing as the question is as follows:

> What is the **lowest** location number that corresponds to any of the initial seed numbers?

Basically, we need to find the matching soil numbers for the seeds, and match our way to the location column. We can construct the ranges and then find the intersect, before proceeding to the next step and using the same approach.

I’m not sure if I can define a recursive function, seeing as there’s the bit about identifying non-mapped source values and using those same values for the destination.