module Main where

import Data.List.Split (splitOn)
import Debug.Trace (trace)

type Source = Integer
type Target = Integer
type Range = Integer

main :: IO ()
main = do
    raw <- splitOn "\n\n" <$> readFile "./data/day_05.txt"

    let seedsPart1 = map read . tail . words $ head raw :: [Integer]
        seedsPart2 = mconcat . map expandSeedRange $ seedPairs seedsPart1 :: [Integer]
        parsed = map parse $ tail raw :: [(String, [(Target, Source, Range)])]

        seedToSoil = snd (head parsed)
        -- matchingSoilNumbers = map (getTargetNum seedToSoil) seedsPart1
        matchingSoilNumbers = map (getTargetNum seedToSoil) seedsPart2
        soilToFertiliser = snd (parsed!!1)
        matchingFertiliserNumbers = map (getTargetNum soilToFertiliser) matchingSoilNumbers
        fertiliserToWater = snd (parsed!!2)
        matchingWaterNumbers = map (getTargetNum fertiliserToWater) matchingFertiliserNumbers
        waterToLight = snd (parsed!!3)
        matchingLightNumbers = map (getTargetNum waterToLight) matchingWaterNumbers
        lightToTemperature = snd (parsed!!4)
        matchingTemperatureNumbers = map (getTargetNum lightToTemperature) matchingLightNumbers
        temperatureToHumidity = snd (parsed!!5)
        matchingHumidityNumbers = map (getTargetNum temperatureToHumidity) matchingTemperatureNumbers
        humidityToLocation = snd (parsed!!6)
        matchingLocationNumbers = map (getTargetNum humidityToLocation) matchingHumidityNumbers

    -- Part 1 (662197086)
    -- print $ minimum matchingLocationNumbers
    -- Part 2 (52510809)
    print $ minimum matchingLocationNumbers

parse :: String -> (String, [(Target, Source, Range)])
parse rawLine = (label, values)
  where
    splitRawLine = splitOn ":\n" rawLine :: [String]
    label = head splitRawLine :: String
    values = map (threeListToTuple . map read . words) . lines $ last splitRawLine :: [(Target, Source, Range)]

    threeListToTuple :: [Integer] -> (Target, Source, Range)
    threeListToTuple [] = (0, 0, 0)
    threeListToTuple xs = (head xs, xs!!1, last xs)

evalSourceNum :: Integer -> (Target, Source, Range) -> Bool
evalSourceNum sourceNum (t, s, r) = sourceNum >= s && sourceNum <= (s + r - 1)

getTargetNum :: [(Target, Source, Range)] -> Integer -> Integer
getTargetNum mappings sourceNum
    | null mappingOfInterest = sourceNum
    | otherwise = targetNum sourceNum (head mappingOfInterest)
  where
    mappingOfInterest = filter (evalSourceNum sourceNum) mappings

    targetNum :: Integer -> (Target, Source, Range) -> Integer
    targetNum sourceNum (t, s, r) = t + (sourceNum - s)

seedPairs :: [Integer] -> [(Integer, Integer)]
seedPairs [] = []
seedPairs xs = pairListToTuple (take 2 xs) : seedPairs (drop 2 xs)
  where
    pairListToTuple :: [Integer] -> (Integer, Integer)
    pairListToTuple xs = (head xs, last xs)

expandSeedRange :: (Integer, Integer) -> [Integer]
expandSeedRange (seedStart, range) = [seedStart .. seedStart + range - 1]