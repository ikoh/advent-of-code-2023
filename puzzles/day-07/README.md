# AoC 2023 Day 7

## Assigning a hand to a type

A hand has **five cards**, like below:

```
32T3K
```

A hand can be one of five types, with the strength of the types in the following decreasing order:

- Five of a kind
- Four of a kind
- Full house
- Three of a kind (the difference between this and a full house is that, in a full house, the remaining two cards are the same, whereas in this type they are different)
- Two pair
- One pair
- High card (all cards are distinct)

Each type thus has a ‘signature’ that could be used to identify it:

- `[5]` – five of a kind
- `[4,1]` – four of a kind
- `[3,2]` – full house
- `[3,1,1]` – three of a kind
- `[2,2,1]` – two pair
- `[2,1,1,1]` – one pair
- `[1,1,1,1,1]` – high card

Going back to our example, we need to take an input `32T3K` and return some signature (in this case `[2,1,1,1]` or a one pair). Example:

```
ghci> import Data.List
ghci> reverse . sort . map length . group $ sort "32T3K"
[2,1,1,1]
```

We’d then give the hand its assigned type:

```haskell
data HandType
    = HighCard
    | OnePair
    | TwoPair
    | ThreeKind
    | FullHouse
    | FourKind
    | FiveKind
    deriving (Eq, Ord, Show)
```