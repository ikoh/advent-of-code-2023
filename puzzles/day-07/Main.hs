module Main where

import Data.List (group, sort)

data HandType
    = NoHand
    | HighCard
    | OnePair
    | TwoPair
    | ThreeKind
    | FullHouse
    | FourKind
    | FiveKind
    deriving (Eq, Ord, Show)

data Card
    = Zero
    | Two
    | Three
    | Four
    | Five
    | Six
    | Seven
    | Eight
    | Nine
    | Ten
    | Jack
    | Queen
    | King
    | Ace
    deriving (Eq, Ord, Show)

main :: IO ()
main = do
    raw <- readFile "./data/day_07.txt"
    undefined

assignHandToType :: String -> HandType
assignHandToType hand =  signature . reverse . sort . map length . group $ sort hand
  where
    signature :: [Int] -> HandType
    signature hand
        | hand == [5]         = FiveKind
        | hand == [4,1]       = FourKind
        | hand == [3,2]       = FullHouse
        | hand == [3,1,1]     = ThreeKind
        | hand == [2,2,1]     = TwoPair
        | hand == [2,1,1,1]   = OnePair
        | hand == [1,1,1,1,1] = HighCard
        | otherwise           = NoHand

-- Write a function that turns a hand from [Char] to [Card]; a list containing multiple
-- [Card] values can then be sorted using the sort function