module Main where

type Duration = Int
type Distance = Int

main :: IO ()
main = do
    rawPart1 <- map (map read . tail . words) . lines <$> readFile "./data/day_06.txt" :: IO [[Int]]
    rawPart2 <- map (read . mconcat . tail . words) . lines <$> readFile "./data/day_06.txt" :: IO [Int]

    let parsedPart1 = zip (head rawPart1) (last rawPart1) :: [(Duration, Distance)]
        parsedPart2 = (head rawPart2, last rawPart2) :: (Duration, Distance)

    -- Part 1 (138915)
    print . product $ map countWinningWays parsedPart1
    -- Part 2 (27340847)
    print $ countWinningWays parsedPart2

countWinningWays :: (Duration, Distance) -> Int
countWinningWays (d, maxDist) = length . filter (> maxDist) $ map (getDistanceTravelled d) chargeTimes
  where
    chargeTimes = [1 .. d] :: [Duration]

    getDistanceTravelled :: Duration -> Int -> Distance
    getDistanceTravelled d chargeTime = (d - chargeTime) * chargeTime