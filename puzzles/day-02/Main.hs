module Main where

import Data.List.Split (splitOn)

type GameId = Int
type RawGameSet = String

main :: IO ()
main = do
    raw <- readFile "./data/day_02.txt"
    let rawSplit = lines raw :: [String]

    -- Part 1 (2617)
    print . sum $ map (solvePart1 . parse) rawSplit
    -- Part 2 (59795)
    print . sum $ map (solvePart2 . parse) rawSplit

parse :: RawGameSet -> (GameId, [[String]])
parse oneLine = (gameIdParsed, colourCounts)
  where
    rawIdSeparated = splitOn ": " oneLine :: [String]
    gameIdParsed = read . drop 5 $ head rawIdSeparated :: GameId

    rawGames = splitOn "; " $ last rawIdSeparated :: [String]
    colourCounts = mconcat $ map (map words . splitOn ", ") rawGames :: [[String]]

getCubeCountsPerColour :: [[String]] -> String -> [Int]
getCubeCountsPerColour xs colour = map (read . head) $ filter (\pair -> last pair == colour) xs

solvePart1 :: (GameId, [[String]]) -> Int
solvePart1 (id, colourCounts) = getReturnValue (id, or [redCompare, greenCompare, blueCompare])
  where
    redConstraint = 12
    greenConstraint = 13
    blueConstraint = 14

    redCompare = maximum (getCubeCountsPerColour colourCounts "red") > redConstraint :: Bool
    greenCompare = maximum (getCubeCountsPerColour colourCounts "green") > greenConstraint :: Bool
    blueCompare = maximum (getCubeCountsPerColour colourCounts "blue") > blueConstraint :: Bool

    getReturnValue :: (GameId, Bool) -> Int
    getReturnValue (id, cubeCountExceedsLimit) = case (id, cubeCountExceedsLimit) of
        (id, False) -> id
        (_, True) -> 0

solvePart2 :: (GameId, [[String]]) -> Int
solvePart2 (id, colourCounts) = product [maxRed, maxGreen, maxBlue]
  where
    maxRed = maximum (getCubeCountsPerColour colourCounts "red") :: Int
    maxGreen = maximum (getCubeCountsPerColour colourCounts "green") :: Int
    maxBlue = maximum (getCubeCountsPerColour colourCounts "blue") :: Int