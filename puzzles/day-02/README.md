# AoC 2023 Day 2

For **Part 1**, the constraints are as follows:

- **12** red cubes
- **13** green cubes
- **14** blue cubes

A raw line looks like this:

```
"Game 100: 9 blue, 8 red, 16 green; 3 red, 7 green, 8 blue; 1 green, 3 red, 12 blue; 3 green, 14 blue"
```

After applying the `parse` function it should look like this:

```
(100, ["9 blue, 8 red, 16 green","3 red, 7 green, 8 blue","1 green, 3 red, 12 blue","3 green, 14 blue"])
```