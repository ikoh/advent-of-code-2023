module Main where

import qualified Data.HashMap.Strict as HM (HashMap, findWithDefault, fromList, keys)
import Data.List (intersect, nub)
import Data.List.Split (splitOn)
import Debug.Trace (trace)

type RawCard = [String]
type CardNumber = Int
type Intersections = Int

main :: IO ()
main = do
    raw <- map (splitOn " | ") . lines <$> readFile "./data/day_04.txt" :: IO [[String]]

    let cardLookup = HM.fromList $ map getCardNumAndIntersections raw :: HM.HashMap CardNumber Intersections
        cardNumbers = HM.keys cardLookup :: [CardNumber]

    -- Part 1 (18653)
    print . sum $ map getScorePerCard raw
    -- Part 2 (5921508)
    print . sum $ map (getCountsOfCopies cardLookup [] []) cardNumbers

getScorePerCard :: RawCard -> Integer
getScorePerCard rawcard
    | length (winningVals `intersect` playerVals) == 0 = 0
    | otherwise = 2 ^ (length (winningVals `intersect` playerVals) - 1)
  where
    winningVals = map read . words . last . splitOn ": " $ head rawcard :: [Int]
    playerVals = map read . words $ last rawcard :: [Int]

getCardNumAndIntersections :: [String] -> (CardNumber, Intersections)
getCardNumAndIntersections rawcard
    | intersections == 0 = (cardNumber, 0)
    | otherwise = (cardNumber, intersections)
  where
    cardNumber = read . last . words . head . splitOn ": " $ head rawcard :: Int
    winningVals = map read . words . last . splitOn ": " $ head rawcard :: [Int]
    playerVals = map read . words $ last rawcard :: [Int]
    intersections = length (winningVals `intersect` playerVals) :: Int

returnImmediateCopies :: HM.HashMap CardNumber Intersections -> CardNumber -> [CardNumber]
returnImmediateCopies lookupMap n
    | copies == 0 = []
    | otherwise   = [n + 1 .. n + copies]
  where
    copies = HM.findWithDefault 0 n lookupMap

getCountsOfCopies :: HM.HashMap CardNumber Intersections -> [CardNumber] -> [CardNumber] -> CardNumber -> Int
getCountsOfCopies cardLookup accumulator forNextRound initialCard
    | not (null accumulator) && null forNextRound = length (accumulator <> [initialCard])
    | null accumulator && null addToNextRoundSingle = length [initialCard]
    -- Without the above case, there would be an infinite recursion because the below case may return an empty list,
    -- which would then trigger the case again and again
    | null accumulator && null forNextRound =
        getCountsOfCopies cardLookup addToNextRoundSingle addToNextRoundSingle initialCard
    | otherwise =
        getCountsOfCopies cardLookup (accumulator <> addToNextRoundMultiple) addToNextRoundMultiple initialCard
  where
    addToNextRoundSingle = returnImmediateCopies cardLookup initialCard
    addToNextRoundMultiple = mconcat $ map (returnImmediateCopies cardLookup) forNextRound