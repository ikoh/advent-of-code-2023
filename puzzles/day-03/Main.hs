module Main where

import qualified Data.Set as Set (fromList, toList)
import Data.List (intersect)
import Debug.Trace (trace)

type Coord = (Int, Int)

main :: IO ()
main = do
    raw <- mconcat . lines <$> readFile "./data/day_03.txt" :: IO String
    let grid = (,) <$> [1 .. 140] <*> [1 .. 140] :: [Coord]
        schematic = zip grid raw :: [(Coord, Char)]

        symbolCoords = map fst $ filter (\(x, y) -> y `notElem` ".0123456789") schematic :: [Coord]
        digitCoords = filter (\(x, y) -> y `elem` "0123456789") schematic :: [(Coord, Char)]

        fullNumbers :: [(Int, [Coord])]
        fullNumbers = buildNumbers digitCoords ([snd $ head digitCoords], [fst $ head digitCoords]) []
        validateNumberPreloaded :: (Int, [Coord]) -> Int
        validateNumberPreloaded = validateNumber symbolCoords

        gearCoords = map fst $ filter (\(x, y) -> y == '*') schematic :: [Coord]
        gearAdjacentCoords = map (filter validateCoords . adjacentCoordsFinder) gearCoords :: [[Coord]]

    -- Part 1 (531561)
    print . sum . map validateNumberPreloaded $ getAdjacentCoordinatesOfNumbers fullNumbers
    -- Part 2 (83279367)
    print . sum . map product . filter (\xs -> length xs == 2) $ map (numsTouchingGears fullNumbers) gearAdjacentCoords

-- Use digitCoords as the first argument
-- Second parameter serves as storage for numbers that aren’t fully formed
buildNumbers :: [(Coord, Char)] -> (String, [Coord]) -> [(Int, [Coord])] -> [(Int, [Coord])]
buildNumbers [] _ fullNumAccumulator = fullNumAccumulator
buildNumbers [oneCoord] (partialNum, partialNumCoords) fullNumAccumulator =
    buildNumbers []
    (partialNum, partialNumCoords)
    fullNumAccumulator <> [(read partialNum, partialNumCoords)]
buildNumbers (coord1:coord2:rest) (partialNum, partialNumCoords) fullNumAccumulator
    | getYAxisDiff coord1 coord2 == 1 =
        buildNumbers (coord2:rest)
        (partialNum <> [snd coord2], partialNumCoords <> [fst coord2])
        fullNumAccumulator
    | getYAxisDiff coord1 coord2 /= 1 =
        buildNumbers (coord2:rest)
        ([snd coord2], [fst coord2])
        fullNumAccumulator <> [(read partialNum, partialNumCoords)]
  where
    getYAxisDiff :: (Coord, Char) -> (Coord, Char) -> Int
    getYAxisDiff ((_, y1), _) ((_, y2), _) = y2 - y1

adjacentCoordsFinder :: Coord -> [Coord]
adjacentCoordsFinder (r, c) = [
    (r, c-1), (r, c+1),
    (r+1, c-1), (r+1, c), (r+1, c+1),
    (r-1, c-1), (r-1, c), (r-1, c+1)
    ]

validateCoords :: Coord -> Bool
validateCoords (r, c)
    | r > 0 && r <= 140 && c > 0 && c <= 140 = True
    | otherwise = False

getAdjacentCoordinatesOfNumbers :: [(Int, [Coord])] -> [(Int, [Coord])]
getAdjacentCoordinatesOfNumbers numsAndCoords = map linkNumToAdjacentCoords numsAndCoords
  where
    getAdjacentCoords :: [Coord] -> [Coord]
    getAdjacentCoords = filter validateCoords . mconcat . map adjacentCoordsFinder

    linkNumToAdjacentCoords :: (Int, [Coord]) -> (Int, [Coord])
    linkNumToAdjacentCoords (n, coords) = (
        n,
        Set.toList . Set.fromList $ getAdjacentCoords coords
        )

validateNumber :: [Coord] -> (Int, [Coord]) -> Int
validateNumber symbols coord
    | length (intersect (snd coord) symbols) > 0 = fst coord
    | otherwise = 0

numsTouchingGears :: [(Int, [Coord])] -> [Coord] -> [Int]
numsTouchingGears numbers gearAdjacentCoords = map fst $ filter (findNumAdjacentToGear gearAdjacentCoords) numbers
  where
    findNumAdjacentToGear :: [Coord] -> (Int, [Coord]) -> Bool
    findNumAdjacentToGear gearAdjacentCoords (n, coord) = length (intersect coord gearAdjacentCoords) > 0