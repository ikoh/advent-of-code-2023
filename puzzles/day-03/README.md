# AoC 2023 Day 3

The grid is 140 by 140 in dimension. Grid creation using applicatives (taken from [here](https://wjwh.eu/posts/2022-11-30-haskell-aoc-tricks.html)):

```
(,) <$> [1 .. 140] <*> [1 .. 140]
```

Example:

```
ghci> (,) <$> [1..3] <*> [1..3]
[(1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3)]
```

## Part 1

- Build numbers from coordinates
- For every number, get the list of adjacent coordinates
- Compare every coordinate with the symbol coordinate list
- Include the number only if at least one of its adjacent coordinates is in the symbol coordinate list

## Part 2

- Get the coordinates of all `*` symbols
- Get the adjacent coordinates of all `*` symbols as a list of lists
- Within each list, the task is to identify two numbers’ coordinates that are found within the list
    - Each number currently has the signature `(Int, [Coord])`
- Lists meeting the above criterion should return the two numbers, then multiply them